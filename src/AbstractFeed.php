<?php

namespace WesternInvestment\FeedManager;

use WesternInvestment\FeedManager\Models\FeedItem;

abstract class AbstractFeed
{
    /** @var string */
    protected $url;

    /** @var \Symfony\Component\Console\Helper\ProgressBar */
    protected $progressBar;

    /**
     * @param int|null $limit
     */
    public function handle(int $limit = null)
    {
        $this->getFeed($limit);
    }

    /**
     * @param int|null $limit
     */
    public function getFeed(int $limit = null)
    {
        $items = $this->getItems();

        if ($this->progressBar) {
            $steps = is_null($limit) ? count($items) : $limit;
            $this->progressBar->setMaxSteps($steps);
        }

        $iteration = 1;

        foreach ($items as $item) {
            if (!is_null($limit) && $iteration > $limit) break;

            if ($this->progressBar) {
                $this->progressBar->advance();
            }

            $iteration++;
            if ($this->exists($item->{$this->getIdField()})) continue;

            $this->createEntry($item);
        }
    }

    /**
     * @return string
     */
    public function getIdField()
    {
        return "id";
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param $parameter
     *
     * @return bool
     */
    public abstract function exists($parameter): bool;

    /**
     * @param $item
     * @param array $attributes
     */
    public abstract function createEntry($item, array $attributes = []);

    /**
     * @return mixed
     */
    public abstract function getItems();

    /**
     * @param \Symfony\Component\Console\Helper\ProgressBar|null $bar
     *
     * @return $this
     */
    public function withProgressBar(\Symfony\Component\Console\Helper\ProgressBar $bar = null)
    {
        $this->progressBar = $bar;

        return $this;
    }

    /**
     * @param $html
     *
     * @return string
     */
    public function sanitizeHtml($html)
    {
        $html = preg_replace('/\s+/', ' ', $html);
        $html = preg_replace('/ class="MsoNormal"/', '', $html);
        $html = preg_replace('/<div id="image_naujiena">(.*)<\/div>/', '', $html);
        $html = html_entity_decode($html);

        return trim($html);
    }
}
