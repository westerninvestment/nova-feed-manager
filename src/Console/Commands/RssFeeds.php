<?php

namespace WesternInvestment\FeedManager\Console\Commands;

use WesternInvestment\FeedManager\FeedManager;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use WesternInvestment\FeedManager\RssFeed;

class RssFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:feeds 
                            {--progress : Show progress bar} 
                            {--all : Run all feeds at once} 
                            {--disabled : Include disabled feeds}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all rss feeds';

    private $progressBar;

    public function handle()
    {
        /** @var FeedManager $manager */
        $manager = app(FeedManager::class);

        $feeds = $this->option('disabled')
            ? $manager->getFeeds()
            : $manager->getActiveFeeds();

        if ($this->option('all')) {
            foreach ($feeds as $feed) {
                $this->runFeed($feed);
            }

            exit;
        }

        $key = $this->choice("Which feed you would like to run?", $feeds->pluck('title', 'key')->toArray());

        if ($this->option('progress')) {
            $this->progressBar = $this->output->createProgressBar();
        }

        $feed = $manager->getFeed($key);
        $this->runFeed($feed);
    }

    public function runFeed(RssFeed $feed)
    {
        $this->info("Importing feed \"{$feed->title}\"...");
        try {
            $feed->run(null, $this->progressBar);
        } catch (\Exception $exception) {
            Log::error("RSS feed import error:" . $exception->getMessage());
            $this->error("Failed! {$exception->getMessage()}");
        }

        $this->info("Done!\n");
    }
}
