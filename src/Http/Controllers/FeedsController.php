<?php

namespace WesternInvestment\FeedManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use WesternInvestment\FeedManager\FeedManager;
use WesternInvestment\FeedManager\Models\FeedItem;

class FeedsController extends Controller
{
    /**
     * @var FeedManager
     */
    private $manager;

    public function __construct(FeedManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return Collection
     */
    public function index()
    {
        return $this->manager->getFeeds();
    }

    /**
     * @param Request $request
     * @param $feed
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $feed)
    {
        $feed = $this->manager->getFeed($feed);
        $active = (bool) $request->input('active');

        $feed->setActive($active);
        FeedItem::query()->updateOrCreate([
            'key' => $feed->key,
        ], [
            'active' => $active,
        ]);

        return $active ? response('on', 201) : response('off', 204);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function run(Request $request)
    {
        $feed = $this->manager->getFeed($request->input('feed'));

        $feed->run();

        return response('Done.', 200);
    }
}
