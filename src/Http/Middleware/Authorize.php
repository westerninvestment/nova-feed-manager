<?php

namespace WesternInvestment\FeedManager\Http\Middleware;

use WesternInvestment\FeedManager\FeedManagerTool;

class Authorize
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        return resolve(FeedManagerTool::class)->authorize($request) ? $next($request) : abort(403);
    }
}
