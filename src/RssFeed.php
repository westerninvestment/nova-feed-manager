<?php

namespace WesternInvestment\FeedManager;

use WesternInvestment\FeedManager\Models\FeedItem;

class RssFeed
{
    /** @var string */
    public $key;

    /** @var AbstractFeed */
    public $handler;

    /** @var string */
    public $title;

    /** @var bool */
    public $active;

    /** @var */
    public $last_fetched_at;

    /**
     * RssFeed constructor.
     *
     * @param string $key
     * @param AbstractFeed $handler
     * @param string|null $title
     */
    public function __construct(string $key, AbstractFeed $handler, string $title = null)
    {
        $this->key = $key;
        $this->handler = $handler;
        $this->title = $title;
        $this->active = true;
    }

    /**
     * @param null $limit
     * @param null $progress
     */
    public function run($limit = null, $progress = null)
    {
        $this->handler->withProgressBar($progress)->handle($limit);

        FeedItem::query()->updateOrCreate([
            'key' => $this->key,
        ], [
            'last_fetched_at' => now(),
        ]);
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active == true;
    }

    /**
     * @param $lastFetchedAt
     *
     * @return $this
     */
    public function setLastFetchedAt($lastFetchedAt)
    {
        $this->last_fetched_at = $lastFetchedAt;

        return $this;
    }
}
