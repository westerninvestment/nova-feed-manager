<?php

namespace WesternInvestment\FeedManager\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property bool active
 * @property DateTime last_fetched_at
 */
class FeedItem extends Model
{
    protected $table = 'rss_feeds';

    protected $fillable = [
        'key', 'last_fetched_at', 'active'
    ];

    protected $casts = [
        'last_fetched_at' => 'date|Y-m-d',
    ];
}
