<?php

namespace WesternInvestment\FeedManager;

use Illuminate\Translation\Translator;
use Laravel\Nova\Nova;
use Laravel\Nova\Tool;

class FeedManagerTool extends Tool
{
    /**
     * Perform any tasks that need to happen when the tool is booted.
     *
     * @return void
     */
    public function boot()
    {
        Nova::script('feed-manager', __DIR__.'/../dist/js/tool.js');
        Nova::style('feed-manager', __DIR__.'/../dist/css/tool.css');

        /** @var Translator $translator */
        $translator = app('translator');
        $translator->load('*', '*', app()->getLocale());
        $lines = $translator->getFromJson('*');

        Nova::provideToScript([
            'translations' => $lines
        ]);
    }

    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return \Illuminate\View\View
     */
    public function renderNavigation()
    {
        return view('feed-manager::navigation');
    }
}
