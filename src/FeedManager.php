<?php

namespace WesternInvestment\FeedManager;

use Illuminate\Support\Collection;
use WesternInvestment\FeedManager\Models\FeedItem;

class FeedManager
{
    /** @var Collection */
    public $feeds = [];

    public function __construct()
    {
        $this->feeds = collect([]);
    }

    /**
     * @param $feed
     *
     * @return $this
     */
    public function addFeed($feed)
    {
        if (is_array($feed)) {
            $this->feeds = $this->feeds->merge($feed);

            return $this;
        }

        $this->feeds->push($feed);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getFeeds()
    {
        $this->feeds->map(function (RssFeed $feed) {
            /** @var FeedItem $item */
            $item = FeedItem::query()->where('key', $feed->key)->first();

            if ($item) {
                $feed->setActive($item->active);
                $feed->setLastFetchedAt($item->last_fetched_at);
            }

            return $feed;
        });

        return $this->feeds;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getActiveFeeds()
    {
        return $this->getFeeds()->filter(function (RssFeed $item) {
            return $item->isActive();
        });
    }

    /**
     * @param $key
     * @return RssFeed|null
     */
    public function getFeed($key)
    {
        return $this->getFeeds()->firstWhere('key', $key);
    }
}
