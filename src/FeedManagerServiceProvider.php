<?php

namespace WesternInvestment\FeedManager;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;
use WesternInvestment\FeedManager\Console\Commands\RssFeeds;
use WesternInvestment\FeedManager\Http\Middleware\Authorize;

class FeedManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(FeedManager::class, function ()
        {
            return new FeedManager();
        });

        $this->loadMigrationsFrom(__DIR__.'/../migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'feed-manager');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../resources/lang');

        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            Nova::resourcesIn(__DIR__ . '/../resources');
        });

        $this->registerCommands();
    }

    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova', Authorize::class])
            ->namespace('\WesternInvestment\FeedManager\Http\Controllers')
            ->prefix('nova-vendor/feed-manager')
            ->group(__DIR__ . '/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    private function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                RssFeeds::class,
            ]);
        }
    }
}
